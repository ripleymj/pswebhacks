# pswebhacks

Don't use any of this. Bad things could happen, and may have already just for thinking about using this.

Should you make the poor choice to look into this repo, you might find:

* `LinuxFilter` - restrict process forking from WLS using seccomp
* `WindowsFilter` - restrict process forking from WLS using quotas
* `inspect` - discover PeopleSoft web session data
* `logfield` - add OPRID to WebLogic access logs using PPM data
* `logheader` - add OPRID and appserver data to HTTP headers
